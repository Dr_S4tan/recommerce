

GET /marque : liste des marques
GET /marque/id : voir une marque
POST /marque : ajouter une marque
PUT /marque/id : éditer une marque
DELETE /marque/id : supprimer une marque

GET /produit : liste des produits
GET /produit/id : voir un produit
POST /produit : ajouter un produit (ne marche pas)
PUT /produit/id : éditer un produit (ne marche pas)
DELETE /produit/id : supprimer un produit