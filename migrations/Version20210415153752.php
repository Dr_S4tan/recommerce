<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210415153752 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE reprise (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, prix DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reprise_produit (reprise_id INT NOT NULL, produit_id INT NOT NULL, INDEX IDX_1B2E6A8FD4147634 (reprise_id), INDEX IDX_1B2E6A8FF347EFB (produit_id), PRIMARY KEY(reprise_id, produit_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reprise_produit ADD CONSTRAINT FK_1B2E6A8FD4147634 FOREIGN KEY (reprise_id) REFERENCES reprise (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reprise_produit ADD CONSTRAINT FK_1B2E6A8FF347EFB FOREIGN KEY (produit_id) REFERENCES produit (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE reprise_produit DROP FOREIGN KEY FK_1B2E6A8FD4147634');
        $this->addSql('DROP TABLE reprise');
        $this->addSql('DROP TABLE reprise_produit');
    }
}
