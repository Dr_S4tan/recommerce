<?php

namespace App\Entity;

use App\Repository\ProduitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=ProduitRepository::class)
 * @UniqueEntity(fields={"Marque", "nom"})
 */
class Produit
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"produit_list","produit_show"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"produit_list","produit_show"})
     */
    private $nom;

    /**
     * @ORM\Column(type="float")
     * @Groups({"produit_list","produit_show"})
     */
    private $prix;

    /**
     * @ORM\ManyToOne(targetEntity=Marque::class, inversedBy="produits")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"produit_list","produit_show"})
     */
    private $Marque;

    /**
     * @ORM\ManyToMany(targetEntity=Reprise::class, mappedBy="Produit")
     */
    private $reprises;

    public function __construct()
    {
        $this->reprises = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getMarque(): ?Marque
    {
        return $this->Marque;
    }

    public function setMarque(?Marque $Marque): self
    {
        $this->Marque = $Marque;

        return $this;
    }

    /**
     * @return Collection|Reprise[]
     */
    public function getReprises(): Collection
    {
        return $this->reprises;
    }

    public function addReprise(Reprise $reprise): self
    {
        if (!$this->reprises->contains($reprise)) {
            $this->reprises[] = $reprise;
            $reprise->addProduit($this);
        }

        return $this;
    }

    public function removeReprise(Reprise $reprise): self
    {
        if ($this->reprises->removeElement($reprise)) {
            $reprise->removeProduit($this);
        }

        return $this;
    }
}
