<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ProduitController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/produit", name="produit_list" ,methods={"GET"})*********************************************************
     ******************************************************************************************************************/
    public function index(SerializerInterface $serializer, ProduitRepository $produitRepository): Response
    {
        $Produits = $produitRepository->findAll();
        $data = $this->get('serializer')->serialize($Produits, 'json',['groups' => 'produit_list']);

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/produit/{id}", name="produit_show" ,methods={"GET"})****************************************************
     ******************************************************************************************************************/
    public function show(SerializerInterface $serializer, Produit $Produit): Response
    {
        $data = $this->get('serializer')->serialize($Produit, 'json',['groups' => 'produit_show']);

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/produit", name="produit_add" ,methods={"POST"})*********************************************************
     ******************************************************************************************************************/
    public function add(SerializerInterface $serializer, Request $request): Response
    {
        $data = $request->getContent();

        $Produit = $serializer->deserialize($data, Produit::class, 'json');

        $this->em->persist($Produit);
        $this->em->flush();

        return new Response('', Response::HTTP_CREATED);
    }

    /**
     * @Route("/produit/{id}", name="produit_edit" ,methods={"PUT"})****************************************************
     ******************************************************************************************************************/
    public function edit(SerializerInterface $serializer, Request $request, Produit $Produit): Response
    {
        $data = $request->getContent();
        $ProduitEdit = $serializer->deserialize($data, 'App\Entity\Produit', 'json');

        $Produit->setNom($ProduitEdit->getNom());
        $Produit->setPrix($ProduitEdit->getPrix());
        $Produit->setMarque($ProduitEdit->getMarque());
        $this->em->persist($Produit);
        $this->em->flush();

        return new Response('', Response::HTTP_OK);
    }

    /**
     * @Route("/produit/{id}", name="produit_delete" ,methods={"DELETE"})***********************************************
     ******************************************************************************************************************/
    public function delete(SerializerInterface $serializer, Request $request, Produit $Produit): Response
    {
        $this->em->remove($Produit);
        $this->em->flush();

        return new Response('', Response::HTTP_OK);
    }
}
