<?php

namespace App\Controller;

use App\Entity\Marque;
use App\Repository\MarqueRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class MarqueController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/marque", name="marque_list" ,methods={"GET"})***********************************************************
     ******************************************************************************************************************/
    public function index(SerializerInterface $serializer, MarqueRepository $marqueRepository): Response
    {
        $Marques = $marqueRepository->findAll();

        $data = $this->get('serializer')->serialize($Marques, 'json',['groups' => 'marque_list']);

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/marque/{id}", name="marque_show" ,methods={"GET"})******************************************************
     ******************************************************************************************************************/
    public function show(SerializerInterface $serializer, Marque $Marque): Response
    {
        $data = $this->get('serializer')->serialize($Marque, 'json',['groups' => 'marque_show']);

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/marque", name="marque_add" ,methods={"POST"})***********************************************************
     ******************************************************************************************************************/
    public function add(SerializerInterface $serializer, Request $request): Response
    {
        $data = $request->getContent();
        $Marque = $serializer->deserialize($data, 'App\Entity\Marque', 'json');

        $this->em->persist($Marque);
        $this->em->flush();

        return new Response('', Response::HTTP_CREATED);
    }

    /**
     * @Route("/marque/{id}", name="marque_edit" ,methods={"PUT"})**********************************************************
     ******************************************************************************************************************/
    public function edit(SerializerInterface $serializer, Request $request, Marque $Marque): Response
    {
        $data = $request->getContent();
        $MarqueEdit = $serializer->deserialize($data, 'App\Entity\Marque', 'json');

        $Marque->setNom($MarqueEdit->getNom());
        $this->em->persist($Marque);
        $this->em->flush();

        return new Response('', Response::HTTP_OK);
    }

    /**
     * @Route("/marque/{id}", name="marque_delete" ,methods={"DELETE"})*************************************************
     ******************************************************************************************************************/
    public function delete(SerializerInterface $serializer, Request $request, Marque $Marque): Response
    {
        $this->em->remove($Marque);
        $this->em->flush();

        return new Response('', Response::HTTP_OK);
    }
}
